import React from 'react';
import { BrowserRouter, Routes, Route} from 'react-router-dom';
import { BooksContextProvider } from './contexts/booksContext';
import HomePage from './views/pages/home';
import LoginPage from './views/pages/login';

function App() {
  return (
    <BooksContextProvider>
      <BrowserRouter>
      <Routes>
        <Route path="/" element={<LoginPage />} />
        <Route path="/home" element={<HomePage />} />
      </Routes>
      </BrowserRouter>
    </BooksContextProvider>
  );
}

export default App;
