import { ReactNode, useState, createContext} from "react";
import { Book } from "../views/pages/home";

type BooksContextType = {
    modalItem: Book | undefined,
    setModalItem: any,
    page: number,
    setPage: any,
    totalPages: number,
    totalItems: number,
    setTotalItems: any,
    setTotalPages: any,
};
type BooksContextProviderProps = {
    children: ReactNode;
};
export const BooksContext = createContext({} as BooksContextType)

export function BooksContextProvider({children}: BooksContextProviderProps) {
    const [modalItem, setModalItem] = useState<Book | undefined>()
    const [page, setPage] = useState(1)
    const [totalPages, setTotalPages] = useState(1)
    const [totalItems, setTotalItems] = useState(1)


    return (
        <BooksContext.Provider value={{modalItem, setModalItem, page, setPage, totalPages, setTotalPages, totalItems, setTotalItems}} >
            {children}
        </BooksContext.Provider>
    )
}