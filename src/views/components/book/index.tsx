import React, { useContext } from 'react'
import { BooksContext } from '../../../contexts/booksContext'
import { Book } from '../../pages/home'
import './style.css'

type Props = {
  book: Book
}
const BookArea = ({book}: Props) => {
  const {setModalItem} = useContext(BooksContext)
  return (
    <div className='book-area' onClick={() => setModalItem(book)}>
      <div className='img'>
        <img className='card-img' src={book.imageUrl} alt="Livro" />
      </div>
      <div className='card-info'>
        <div>
          <p style={{fontWeight: '500'}}>{book.title}</p>
          <p style={{color: '#AB2680', fontSize: '.85rem'}}>{book.publisher}</p>
        </div>
        <div className='book-details'>
          <p>{book.pageCount} páginas</p>
          <p>Editora Loyola</p>
          <p>Publicado em {book.published}</p>
        </div>
      </div>
    </div>
  )
}

export default BookArea