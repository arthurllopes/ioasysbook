import React, { useContext } from 'react'
import { BooksContext } from '../../../contexts/booksContext'
import './style.css'

const Modal = () => {
  const book = useContext(BooksContext).modalItem
  const {setModalItem} = useContext(BooksContext)
  function handleOutsideClick (event: React.MouseEvent){
    if (event.target === event.currentTarget) setModalItem()
}
  return (
    <div className='container' onClick={handleOutsideClick}>
      <div className='content' >
        <div className='img-div'>
          <img className='modal-img' src={book?.imageUrl} alt="Livro" />
        </div>
        <div className='book-info'>
          <div className='info-area'>
            <h3>{book?.title}</h3>
            <p style={{color: '#AB2680'}}>{book?.publisher}</p>
          </div>
          <div className='info-area'>
            <h5>INFORMAÇÕES</h5>
            <div className="info">
              <div className="info-item">Páginas</div>
              <div className="info-info">{book?.pageCount}</div>
            </div>
            <div className="info">
              <div className="info-item">Editora</div>
              <div className="info-info">Editora Loyola</div>
            </div>
            <div className="info">
              <div className="info-item">Publicação</div>
              <div className="info-info">{book?.published}</div>
            </div>
            <div className="info">
              <div className="info-item">Idioma</div>
              <div className="info-info">{book?.language}</div>
            </div>
            <div className="info">
              <div className="info-item">Título Original</div>
              <div className="info-info">{book?.title}</div>
            </div>
            <div className="info">
              <div className="info-item">ISBN-10</div>
              <div className="info-info">{book?.isbn10}</div>
            </div>
            <div className="info">
              <div className="info-item">ISBN-13</div>
              <div className="info-info">{book?.isbn13}</div>
            </div>
          </div>
          <div className='info-area'>
            <h5>RESENHA DA EDITORA</h5>
            <p className='info-info'>{book?.description}</p>
          </div>
        </div>
        <div className='close-btn' onClick={() => setModalItem()}>x</div>
      </div>
    </div>
  )
}

export default Modal