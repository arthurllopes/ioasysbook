import React, { useContext } from 'react'
import './style.css'
import BookArea from '../../components/book'
import Header from '../../components/header'
import Pagination from '../../components/pagination'
import Modal from '../../components/modal'
import { BooksContext } from '../../../contexts/booksContext'
import { Navigate } from 'react-router-dom'

export type Book = {
  authors: string[],
  category: string,
  description: string,
  id: string,
  imageUrl: string,
  isbn10: string,
  isbn13: string,
  language: string,
  pageCount: number,
  published: number,
  publisher: string,
  title: string,
}
const HomePage = () => {
  const [books, setBooks] = React.useState<Book[]>([])
  const {modalItem, page, setTotalItems, setTotalPages} = useContext(BooksContext)
  const token =localStorage.getItem('accessToken')?.replaceAll('"', '')

  //caso usuario passe muito rapidamente a pagina. Pra evitar que nao faça requisicoes desnecessarias ate o usuario chegar na pag desejada
  const timeoutRef = React.useRef()
  const [loading, setLoading] = React.useState(false)

  React.useEffect(() => {
    clearTimeout(timeoutRef.current)
    const getBooks = async () => {
      setLoading(true)
      const response = await fetch(`https://books.ioasys.com.br/api/v1/books?page=${page}&amount=12`, {
          method: 'GET',
          headers: {
            Authorization: 'Bearer ' + token,
          },
        }
      ).then(response => response.json())
      const {data, totalItems, totalPages} =  response
      setTotalItems(totalItems)
      setTotalPages(totalPages)

      setBooks(data)
      setLoading(false)
    }
    //@ts-ignore
    timeoutRef.current = setTimeout(() => {
      getBooks()
    }, 500)
  }, [page, setTotalPages, setTotalItems, token])
  if (!token) {
    return (
      <Navigate to="/" replace={true} />
    )
  } else {
    return (
      <>
      <Header />
      {loading ? (
          <main>
            Carregando...
          </main>
          ) : (
          <>
            <div className='page'>
              
              <main>
                {books.map((book: Book) => (
                  <BookArea key={book.id} book={book}/>
                ))}
              </main>
              <Pagination />
            </div>
            {modalItem && <Modal />}
          </>
        )}
      
      </>
    )
  }
}

export default HomePage