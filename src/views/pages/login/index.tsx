import React, { FormEvent } from 'react'
import { useNavigate } from "react-router-dom";
import './style.css'

type User = {
  email: string,
  password: string
}
const LoginPage = () => {
  const navigate = useNavigate()
  const [error, setError] = React.useState<string>('')
  const [credentials, setCredentials] = React.useState<User>({email: '', password: ''})
  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault()
    try {
      const response = await fetch('https://books.ioasys.com.br/api/v1/auth/sign-in', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials)
        }
      )
      const data = await response.json()
      const token = response.headers.get('authorization')
      if (response.status !== 200) {
        throw new Error(data.errors.message);
      }
      localStorage.setItem('accessToken', JSON.stringify(token))
      localStorage.setItem('userData', JSON.stringify(data))
      navigate('/home')

    } catch (error: any) {
      setError(error?.message)
    }
  }
  function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    const {name, value} = e.target
    setCredentials({...credentials, [name]: value})
  }
  return (
    <main className="page-wrapper">
      <div className="form-content">
        <div>
          <p className='logo-text'>ioasys <span className="book-text">Books</span></p>
        </div>
        <form onSubmit={handleSubmit}>
          <div className='input-div'>
            <div className='input-info' >
              <label htmlFor="email">Email</label>
              <input type="text" id="email" name='email' placeholder='desafio@ioasys.com.br' value={credentials?.email} onChange={(e) => handleChange(e)} required/>
            </div>
          </div>
          <div className='input-div'>
            <div className='input-info'>
              <label htmlFor="password">Senha</label>
              <input type="password" name="password" id="password" placeholder='12341234' value={credentials.password} onChange={(e) => handleChange(e)} required/>
            </div>
            <div className=''>
              <button className='login-btn' type="submit">Entrar</button>
            </div>
          </div>
        </form>
        {error && <div className='tooltip'>
          {error}
          <div className='tooltip-arrow'></div>
        </div>}
      </div>
    </main>
  )
}

export default LoginPage