# Ioasys desafio técnico Front End
Aplicação para ver listagens de livros.</br>
Link da demo: https://flamboyant-cray-c72c95.netlify.app/

## Projeto Overview / Work Flow
Comecei esse projeto organizando a estrutura básica de pastas e arquivos de acordo com o design. Depois de separar os componentes, fiz o layout base. Adicionei a ContextAPI para funções básicas pelo layout ser componentizado.

![Ioasys]()

## 💻 Tecnologias
 - [React](https://pt-br.reactjs.org/)
 - [Typescript](https://www.typescriptlang.org/)
 - [React Router Dom](https://v5.reactrouter.com/web/guides/quick-start)
 

# Instalação e uso

### **Clone do projeto**

```bash
# Abra um terminal e copie este repositório com o comando
$ git clone https://arthurllopes@bitbucket.org/arthurllopes/ioasysbook.git
# Entre na pasta do repositório clonado
$ cd ioasysbook
```

### **Iniciando o projeto**

```
# Instale as dependências
npm install

# Rode a aplicação
npm start
```
<br>


## Autor
Feito por Arthur Lopes 👋🏽 Entre em contato!